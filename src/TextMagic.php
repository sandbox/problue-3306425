<?php

namespace Drupal\textmagic;

/**
 * @file
 * Configures the TextMagic API for use as a service.
 */

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\AlertCommand;
use TextMagic\Api\TextMagicApi;
use TextMagic\Configuration;
use TextMagic\Models\SendMessageInputObject;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

/**
 * The TextMagic API service.
 */
 
class TextMagic {
	
  /**
   * Configures the API.
   */	
  public function api() {
		// Get the username and API key from the settings fom
    $config = Configuration::getDefaultConfiguration()
    ->setUsername(\Drupal::config('textmagic.settings')->get('username'))
    ->setPassword(\Drupal::config('textmagic.settings')->get('apikey'));
	
    $api = new TextMagicApi(
      new Client(),
      $config
    );
    return $api;
  }

  /**
   * Send a text message.
   */
  public function send($message, $recipients) {
    $input = new SendMessageInputObject();
    $input->setText($message);
    $input->setPhones($recipients);
	
	
	$result = $this->api()->sendMessage($input);
  }
}


