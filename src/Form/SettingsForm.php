<?php

namespace Drupal\textmagic\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure TextMagic settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'textmagic_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['textmagic.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['username'] = [
      '#type' => 'textfield',
      '#title' => $this->t('TextMagic API Username'),
      '#default_value' => $this->config('textmagic.settings')->get('username'),
    ];
	$form['apikey'] = [
      '#type' => 'textfield',
      '#title' => $this->t('TextMagic API Key'),
      '#default_value' => $this->config('textmagic.settings')->get('apikey'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('textmagic.settings')
      ->set('username', $form_state->getValue('username'))
      ->save();
	$this->config('textmagic.settings')
      ->set('apikey', $form_state->getValue('apikey'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
