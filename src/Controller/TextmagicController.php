<?php

namespace Drupal\textmagic\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\textmagic\TextMagic;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Returns responses for TextMagic routes.
 */
class TextmagicController extends ControllerBase {
/**
* @var \Drupal\textmagic\TextMagic
*/
protected $textmagic;

/**
 * TextmagicController Constructor.
 *
 * @param \Drupal\textmagic\TeaxtMagic $api
*/

public function __construct(TextMagic $textmagic) {
  $this->textmagic = $textmagic;
}

/**
 * {@inheritdoc}
 */
public static function create(ContainerInterface $container) {
  return new static(
    $container->get('textmagic.textmagic')
  );
}

  /**
   * Builds the response.
   */
  public function build() {

  $message = 'This is a test message';
  $recipients = '447843620518';

    $result = $this->textmagic->send($message, $recipients);

    $build['content'] = [
      '#type' => 'item',
      '#markup' => $this->t('It works!'),
    ];

    return $build;
  }

}
